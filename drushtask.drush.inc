<?php

/**
 * Implementation of hook_drush_command().
 */
function drushtask_drush_command() {
  $items['provision-drush'] = array(
    'arguments' => array('command' => dt('The command to be run on the site.')),
    'description' => dt('run a drush command on a site.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT
  );
  return $items;
}

function drush_drushtask_pre_hosting_task() {
  $task =& drush_get_context('HOSTING_TASK');
  if ($task->ref->type == 'site' && $task->task_type == 'drush') {
    $task->options['command'] = $task->task_args['command'];
  }
}

/**
 * Make sure the site is installed and enabled.
 */
function drush_drushtask_provision_drush_validate() {

  $command = implode(' ', func_get_args());

  if (!@drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION)) {
    if (drush_get_option('force', false)) {
      drush_log("clearing error");
      drush_set_context('DRUSH_ERROR_CODE', DRUSH_SUCCESS);
    }
  }
  if (!drush_get_option('installed') && !drush_get_option('force', false)) {
     drush_set_error('PROVISION_DRUPAL_SITE_NOT_FOUND');
   }

  if(empty($command)) {
    $command = drush_get_option('command');
  }

  if(empty($command)) {
    drush_set_error('PROVISION_DRUSH_NO_COMMAND_GIVEN', dt('Command is empty.'));
  }

  drush_set_option('command', $command);
}

function drush_drushtask_provision_drush() {
  $args = explode(" ", drush_get_option('command'));
  $command = array_shift($args);
  $opening = FALSE;
  $_arg = '';
  $_args = array();
  foreach($args as $arg) {
    $first = substr($arg, 0, 1);
    $last = substr($arg, -1, 1);
    if($opening) {
      $_arg.= ' '.$arg;
      if($last == $opening) {
        $opening = FALSE;
        $_args[] = substr($_arg, 1, -1);
        $_arg = '';
      }
    } elseif(in_array($first, array('"', "'"))) {
       if($last == $first) {
         $_args[] = substr($arg, 1, -1);
       } else {
         $opening = $first;
         $_arg = $arg;
      }
    } else {
      $_args[] = $arg;
    }
  }
  if(!empty($_arg)) {
    $_args[] = $_arg;
  }
  setlocale(LC_CTYPE, "en_US.UTF-8");
  drush_backend_invoke_args($command, $_args);
}

