This is both a frotnend and backend aegir module so it needs
to be accessible from the backend as well as in the frontend.
There are a couple of ways to do this, here are two:

1) Install the module on your frontend. Create a symbolic link 
from AEGIR_HOME/.drush/drushtask.drush.inc to your modules
drushtask.drush.inc

2) Install the module on your frontend. Install the module on
your backend.

